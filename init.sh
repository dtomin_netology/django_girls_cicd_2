FROM python:3
ENV PYTHONUNBUFFERED 1
WORKDIR /code
ADD . /code/
RUN pip3 install -r requirements.txt
CMD sh init.sh && python3 manage.py runserver 0.0.0.0:8000
